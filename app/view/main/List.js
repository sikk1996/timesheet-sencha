/**
 * This view is an example list of people.
 */
Ext.define('Timesheet.view.main.List', {
    extend: 'Ext.grid.Grid',
    xtype: 'mainlist',

    requires: [
        'Timesheet.store.Timesheet'
    ],

    title: 'Timesheet',

    store: {
        type: 'timesheet'
    },

    columns: [
        { text: 'From',  dataIndex: 'name', width: 100 },
        { text: 'To', dataIndex: 'email', width: 230 },
        { text: 'Total Time', dataIndex: 'phone', width: 150 },
        { text: 'Description', dataIndex: 'phone', width: 150 }
    ],

    listeners: {
        select: 'onItemSelected'
    }
});
