Ext.define('Timesheet.store.Timesheet', {
    extend: 'Ext.data.Store',

    alias: 'store.timesheet',

    fields: [
        'dateTimeStart', 'dateTimeEnd', 'totalTime','description'
    ],

    data: { items: [
        { dateTimeStart: 'Jean Luc', dateTimeEnd: "jeanluc.picard@enterprise.com", totalTime: "2:34:23", description: "Malte huset" }
    ]},

    proxy: {
        type: 'memory',
        reader: {
            type: 'json',
            rootProperty: 'items'
        }
    }
});